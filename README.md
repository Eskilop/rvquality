# RvQuality

## Synopsis
A python library that establishes the quality of a review. Based on Luca D'Amato's thesis.

## Code Example
```python
#!/usr/bin/env python3
from rvquality.quality_table import QualityTable
from rvquality.options import Options
import rvquality.components as components
import pandas as pd

main_table = pd.read_csv('path_to_csv_data.csv', sep=';')
# sample id from main table
rv_id = main_table[qt.opts.ID_NAME][0]

# components to include
components = [
  components.C1(), 
  components.C2(), 
  components.C3(), 
  components.C4(), 
  components.C7(), 
  components.C8(),
  components.C9(),
  components.C10(),
  components.C11(),
  components.C12()]

# sets options
opts = Options()
opts.ID_NAME = "id"

quality_table = QualityTable(data_frame)
quality_table.prepare()
qt.quality_of(rv_id, components)
```

## Installation
```bash
# clone the repository
git clone https://gitlab.com/Eskilop/rvquality rvquality

# cd into project directory
cd rvquality

# install
python3 setup.py install
```

## License
```
MIT License

Copyright (c) 2019 Luca D'Amato

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```